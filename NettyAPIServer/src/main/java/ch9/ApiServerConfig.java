package ch9;

import java.net.InetSocketAddress;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration //스프링 설정으로 사용될 클래스임을 명시
@ImportResource("classpath:spring/h2ApplicationContext.xml") //@Configuration 으로 지정된 클래스에서 XML 설정 정보를 사용  
@ComponentScan("ch9, ch9.service, ch9.core") //사용할 컴포넌트의 위치
@PropertySource("classpath:api-server.properties") //설정 프로퍼티
public class ApiServerConfig {
	@Value("${boss.thread.count}")
	private int bossThreadCount;
	
	@Value("${worker.thread.count}")
	private int workerThreadCount;
	
	@Value("${tcp.port}")
	private int tcpPort;
	
	@Bean(name="bossThreadCount") //@Bean은 외부의 라이브러리나 데이터를 통해 객체 생성
	public int getBossThreadCount() {
		return bossThreadCount;
	}
	
	@Bean(name="workerThreadCount")
	public int getWorkerThreadCount() {
		return workerThreadCount;
	}
	
	public int getTcpPort() {
		return tcpPort;
	}
	
	@Bean(name="tcpSocketAddress")
	public InetSocketAddress tcpPort() {
		return new InetSocketAddress(tcpPort);
	}
	
	@Bean 
	// @Value("${property.name}") 형태로 프로퍼티를 사용하기 위해서 필요
	public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
