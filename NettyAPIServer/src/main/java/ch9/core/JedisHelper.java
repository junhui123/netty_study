package ch9.core;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Redis를 자바에서 사용하기 위한 Jedis를 쉽게 사용하기 위한 Helper 클래스
 * @author QWER
 *
 */
public class JedisHelper {
	
	protected static final String REDIS_HOST = "127.0.0.1";
	protected static final int REDIS_PORT = 6379;
	private final Set<Jedis> connectionList = new HashSet<Jedis>();
	private final JedisPool pool;
	
	private JedisHelper() {
		GenericObjectPoolConfig config = new GenericObjectPoolConfig();
		config.setMaxTotal(20);
		config.setBlockWhenExhausted(true);
		
		this.pool = new JedisPool(config, REDIS_HOST, REDIS_PORT);
	}
	
	private static class LazyHolder {
		private static final JedisHelper INSTANCE = new JedisHelper();
	}
	
	public static JedisHelper getInstance() {
		return LazyHolder.INSTANCE;
	}

	//final로 지정된 메소드는 이 클래스를 상속받은 하위 클래스에서 오버라이딩(재정의) 불가
	final public Jedis getConnection() {
		Jedis jedis = this.pool.getResource();
		this.connectionList.add(jedis);
		return jedis;
	}

	final public void returnResource(Jedis jedis) {
		this.pool.returnResource(jedis);
		//jedis.close();
	}
	
	final public void destoryPool() {
		Iterator<Jedis> jedisList = this.connectionList.iterator();	
		while(jedisList.hasNext()) {
			Jedis jedis = jedisList.next();
			this.pool.returnResource(jedis);
			//jedis.close();		}
		}
		this.pool.destroy();
	}
}
