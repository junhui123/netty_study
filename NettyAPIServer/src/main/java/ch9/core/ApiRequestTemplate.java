package ch9.core;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonObject;

import ch9.service.RequestParamException;
import ch9.service.ServiceException;

public abstract class ApiRequestTemplate implements ApiRequest {
	protected Logger logger;
	protected Map<String, String> reqData;
	protected JsonObject apiResult;
	
	public ApiRequestTemplate(Map<String, String> reqData) {
		this.logger = LogManager.getLogger(this.getClass()); 
		this.reqData = reqData;
		this.apiResult = new JsonObject();
	}

	public void executeService() {
		try {
			this.requestParamValidation();
			this.service();
		} catch (RequestParamException e) {
			logger.error(e);
			this.apiResult.addProperty("resultCode", "405");
		} catch (ServiceException e) {
			logger.error(e);
			this.apiResult.addProperty("resultCode", "501");
		}
	}

	public JsonObject getApiResult() {
		return this.apiResult;
	}
	
	//교재에 없는 부분 
    @Override
    public void requestParamValidation() throws RequestParamException {
        if (getClass().getClasses().length == 0) {
            return;
        }
    }
}
