package ch9.core;

/**
 * 발급된 토근을 Redis에 저장/조회 하기 위한 인터페이스
 */
public interface KeyMaker {
	public String getKey();
}
