package ch9.core;

import com.google.gson.JsonObject;

import ch9.service.RequestParamException;
import ch9.service.ServiceException;

/**
 * Template Method 패턴을 사용
 *  상위 클래스에서 처리에 대한 흐름을 제어, 상속 받은 하위에서 처리를 구체화
 *  API 서비스와 종료에 상관없이 동일한 메소드를 통해 동일한 순서로 호출
 *  ApiRequestTemplate과 ApiRequestTemplate을 상속받은 클래스들이 구현
 *  
 */
public interface ApiRequest {
	/**
	 * 지정한 API에서 사용할 파라미터가 입력되어 있는지 확인
	 * @throws RequestParamException
	 */
	public void requestParamValidation() throws RequestParamException;
	
	/**
	 * API 서비스 개별 구현
	 * @throws ServiceException
	 */
	public void service() throws ServiceException;
	
	/**
	 * API 시작을 호출
	 */
	public void executeService();
	
	/**
	 * 처리 결과 확인
	 * @return
	 */
	public JsonObject getApiResult();
}
