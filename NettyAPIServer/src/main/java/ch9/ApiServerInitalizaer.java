package ch9;

import ch9.core.ApiRequestParser;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.ssl.SslContext;

public class ApiServerInitalizaer extends ChannelInitializer<SocketChannel> {
	
	private final SslContext sslCtx;
	
	public ApiServerInitalizaer(SslContext sslCtx) {
		this.sslCtx = sslCtx;
	}

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline p = ch.pipeline();
		if(sslCtx != null) {
			p.addLast(sslCtx.newHandler(ch.alloc()));
		}
		
		p.addLast(new HttpRequestDecoder());
		//HttpObjectAggregator = http 프로토콜 메시지 파편화 처리. 데이터가 나눠서 전송된 경우 데이터를 하나로 합쳐주는 역할
		//생성자에 지정한 숫자는 한 번에 처리 가능한 최대 데이터 크기 현재 65kb. 지정한 숫자 크기 이상의 값이 오면 TooLongFrameException 발생
 		p.addLast(new HttpObjectAggregator(65536));
 		//서버에서 처리 결과를 클라이언트로 전송 시 http 프로토콜로 변환
		p.addLast(new HttpResponseEncoder());
		p.addLast(new HttpContentCompressor());
		//HttpRequestDecoder, HttpObjectAggregator, HttpContentCompressor를 거치면 FullHttpMessage로 변환됨
		//FullHttpMessage를 통해 API 서버를 컨트롤
		p.addLast(new ApiRequestParser());
	}

}
