package ch9.service.DAO;

import ch9.core.KeyMaker;
import redis.clients.util.MurmurHash;

public class TokenKey  implements KeyMaker {
	//MURMURHASH 알고리즘을 위한 해시 시드값
    static final int SEED_MURMURHASH = 0x1234ABCD;

    private String email;
    private long issueDate;

    public TokenKey(String email, long issueDate) {
        this.email = email;
        this.issueDate = issueDate;
    }
    
    @Override
    public String getKey() {
        String source = email + String.valueOf(issueDate);
        return Long.toString(MurmurHash.hash64A(source.getBytes(), SEED_MURMURHASH), 16);
    }
}
