package ch9.service;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import ch9.core.ApiRequestTemplate;

@Service("users") //getBean() 메소드 인자로 사용되어 빈 생성에 사용. CRUD 또는 비즈니스 로직을 처리하는 특정한 컴포넌트를 의미
@Scope("prototype") //스프링 컨텍스트가 객체를 생성 할 때 싱글톤 방식으로 생성할지, 요청시마다 새로 생성할지 지정
					//@Scope 생략 시 @Service, @Component, @Repository 등 자동 스캐닝 빈은 싱글톤 형태로 생성
public class UserInfo extends ApiRequestTemplate {

	@Autowired
	private SqlSession sqlSession;

	public UserInfo(Map<String, String> reqData) {
		super(reqData);
	}

	@Override
	public void requestParamValidation() throws RequestParamException {
		if (StringUtils.isEmpty(this.reqData.get("email"))) {
			throw new RequestParamException("email이 없습니다");
		}
	}

	@Override
	public void service() throws ServiceException {
        Map<String, Object> result = sqlSession.selectOne("users.userInfoByEmail", this.reqData);
        if (result != null) {
            String userNo = String.valueOf(result.get("USERNO"));
            this.apiResult.addProperty("resultCode", "200");
            this.apiResult.addProperty("message", "Success");
            this.apiResult.addProperty("userNo", userNo);
        }
        else {
            this.apiResult.addProperty("resultCode", "404");
        }
	}
}
