package ch9.service;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.JsonObject;

import ch9.core.ApiRequestTemplate;
import ch9.core.JedisHelper;
import ch9.core.KeyMaker;
import ch9.service.DAO.TokenKey;
import redis.clients.jedis.Jedis;

@Service("tokenIssue")
@Scope("prototype")
public class TokenIssue extends ApiRequestTemplate {
	private static final JedisHelper helper = JedisHelper.getInstance();
	
	@Autowired
	private SqlSession sqlSession;
	public TokenIssue(Map<String, String> reqData) {
		super(reqData);
	}

	@Override
	public void requestParamValidation() throws RequestParamException {
		if (StringUtils.isEmpty(this.reqData.get("userNo"))) {
			throw new RequestParamException("userNo가 없습니다");
		}
		
		if (StringUtils.isEmpty(this.reqData.get("password"))) {
			throw new RequestParamException("password가 없습니다");
		}
	}

	@Override
	public void service() throws ServiceException {
        Jedis jedis = null;
		try {
			Map<String, Object> result = sqlSession.selectOne("user.userInfoByPassword", this.reqData);
			if(result==null) {
				final long threeHour = 60*60*3;
				long issuedate = System.currentTimeMillis() / 1000;
				String email = String.valueOf(result.get("USERID"));
				
				JsonObject token = new JsonObject();
				token.addProperty("issueDate", issuedate);
				token.addProperty("expireDate", issuedate+threeHour);
				token.addProperty("email", email);
				token.addProperty("userNo", reqData.get("userNo"));
				
				KeyMaker tokenKey = new TokenKey(email, issuedate);
				jedis = helper.getConnection();
				//지정된 시간 이후 데이터를 자동 삭제
				jedis.setex(tokenKey.getKey(), (int)threeHour, token.toString());
				
				this.apiResult.addProperty("resultCode", "200");
				this.apiResult.addProperty("message", "Success");
				this.apiResult.addProperty("token", tokenKey.getKey());
			} else {
				this.apiResult.addProperty("resultCode", "404");
			}
			helper.returnResource(jedis);
		} catch(Exception e) {
			helper.returnResource(jedis);
		}
	}
}
