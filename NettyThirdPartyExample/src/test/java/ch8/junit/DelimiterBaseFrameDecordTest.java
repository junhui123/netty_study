package ch8.junit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.charset.Charset;

import org.junit.Test;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;

public class DelimiterBaseFrameDecordTest {

	@Test
	public void testDecoder() {
		String writeData = "안녕하세요\r\n반갑습니다\r\n";
		String firstResponse = "안녕하세요\r\n";
		String secondResponse = "반갑습니다\r\n";
		
		//인바운드(클라->서버로 전송) 데이터를 가공하는 클래스 
		DelimiterBasedFrameDecoder decoder = new DelimiterBasedFrameDecoder(8192, false, Delimiters.lineDelimiter());
		//EmbeddedChannel 이벤트 핸들러 테스를 목적으로 하는 클래스. 생성자에 이벤트 핸들러 decoder 등록
		//현재는 인바운더에 대해서 설정되어 있으므로 embeddedChannel.readOutbound() / writeOutbound()는 작동하지 않는다
		EmbeddedChannel embeddedChannel = new EmbeddedChannel(decoder);
		
		ByteBuf request = Unpooled.wrappedBuffer(writeData.getBytes());
		//클라이언트를 통해서 데이터를 수신한것과 같은 상태로 만듬
		boolean result = embeddedChannel.writeInbound(request);
		//클라이언트에를 통해서 데이터를 정삭적으로 수신하여 버퍼에 저장되었는지 확인
		assertTrue(result);
		
		ByteBuf response = null;
		//인바운드(클라->서버)로 전송한 데이터를 읽어 들이는 동작을 진행 
		response = (ByteBuf) embeddedChannel.readInbound();
		assertEquals(firstResponse, response.toString(Charset.defaultCharset()));
		
		response = (ByteBuf) embeddedChannel.readInbound();
		assertEquals(secondResponse, response.toString(Charset.defaultCharset()));
		
		embeddedChannel.finish();
	}
}
