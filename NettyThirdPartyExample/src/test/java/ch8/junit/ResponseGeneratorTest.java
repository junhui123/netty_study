package ch8.junit;

import static org.junit.Assert.*;

import org.junit.Test;

public class ResponseGeneratorTest {

	@Test
	public void testZeroLengthString() {
		String request = "";
		testCall(request);
	}
	
	@Test
	public void testHi() {
		String request = "Hi";
		testCall(request);
	}
	
	@Test
	public void testByeLowerCase() {
		String request = "bye";
		//assertFalse(generator.isClose()) 부분에서 테스트 실패
		//assertFalse는 false로 반환되는지 확인
		//isClose()는  bye와 같으면 true를 반환하므로 테스트 실패 발생 
		testCall(request);
	}
	
	@Test
	public void testByeUpperCase() {
		String request = "BYE";
		testCall(request);
	}
	
	private void testCall(String request) {
		ResponseGenerator generator = new ResponseGenerator(request);
		assertNotNull(generator);
		assertNotNull(generator.response());
		assertNotNull("명령을 입력해 주세요.\r\n", generator.response());
		assertFalse(generator.isClose());		
	}
}
