package Http;

import Util.NettyStartupUtil;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;

public class HttpStaticServer {
	final static String index = System.getProperty("user.dir")+"\\src\\main\\resources\\Http\\index.html";
	public static void main(String[] args) throws Exception{
		NettyStartupUtil.runServer(8020, pipeline -> {
			pipeline.addLast(new HttpServerCodec());
			pipeline.addLast(new HttpObjectAggregator(65536));
			pipeline.addLast(new HttpStaticFileHandler("/", index));
			pipeline.addLast(new HttpNotFoundHandler());
		});
	}
}
