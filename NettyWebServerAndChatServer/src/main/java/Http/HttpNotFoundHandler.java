package Http;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;

public class HttpNotFoundHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

	private String content_type = "text/plain; charset=utf-8";
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest req) throws Exception {
		ByteBuf buf = Unpooled.copiedBuffer("Not Found", CharsetUtil.UTF_8);
		HttpResponse res = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND, buf);
		res.headers().set(HttpHeaderNames.CONTENT_TYPE, content_type);
		if(HttpUtil.isKeepAlive(req)) {
			res.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
		}
		res.headers().set(HttpHeaderNames.CONTENT_LENGTH, buf.readableBytes());
		ctx.writeAndFlush(res).addListener((ChannelFuture f) -> {
			if (!HttpUtil.isKeepAlive(req)) {
				f.channel().close();
			}
		});
	}
}
