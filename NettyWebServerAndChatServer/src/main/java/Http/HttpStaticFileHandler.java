package Http;

import java.io.IOException;
import java.io.RandomAccessFile;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.DefaultFileRegion;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;

public class HttpStaticFileHandler extends SimpleChannelInboundHandler<FullHttpRequest>{

	private final String path;
	private final String filename;
	
	public HttpStaticFileHandler(String path, String filename) {
		super(false); //auto-release false
		this.path = path;
		this.filename = filename;
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest req) throws Exception {
        // TODO: [실습2-1] sendStaticFile메소드를 써서 구현합니다. "/" 요청이 아닌 경우에는 어떻게 할까요?
        // 이 Handler는 SimpleChannelInboundHandler<I>를 확장했지만 "auto-release: false"임에 주의합니다.
        // 상황에 따라 "필요시"에는 retain()을 부르도록 합니다.
		
		HttpRequest request = (HttpRequest) req;
		String uri = request.uri();
		if(uri.equals("/")) {
			sendStaticFile(ctx, req);
		} else {
			ctx.fireChannelRead(req);
		}
	}
	
	private void sendStaticFile(ChannelHandlerContext ctx, FullHttpRequest req) throws IOException {
		try {
			RandomAccessFile raf = new RandomAccessFile(filename, "r");
			long fileLength = raf.length();
			
			HttpResponse res = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
			HttpUtil.setContentLength(res,  fileLength);
			res.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/html; charset=utf-8");
			if(HttpUtil.isKeepAlive(req)) {
				res.headers().set(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.KEEP_ALIVE);
			}
			ctx.write(res); 
			ctx.write(new DefaultFileRegion(raf.getChannel(), 0, fileLength));
			ChannelFuture f = ctx.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);
			if(!HttpUtil.isKeepAlive(req)) {
				f.addListener(ChannelFutureListener.CLOSE);
			}
		} finally {
			req.release();
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
	}
}
