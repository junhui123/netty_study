package WebChat;

import Http.HttpNotFoundHandler;
import Http.HttpStaticFileHandler;
import Util.NettyStartupUtil;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;

public class WebChatServer {
	final static String index = System.getProperty("user.dir")+"\\src\\main\\resources\\WebChat\\index.html";
	public static void main(String[] args) throws Exception{
		NettyStartupUtil.runServer(8040, pipeline-> {
			pipeline.addLast(new HttpServerCodec());
			pipeline.addLast(new HttpObjectAggregator(65536));
			pipeline.addLast(new WebSocketHandshakeHandler("/chat", new WebChatHandler()));
			pipeline.addLast(new HttpStaticFileHandler("/", index));
			pipeline.addLast(new HttpNotFoundHandler());
		});
	}
}
