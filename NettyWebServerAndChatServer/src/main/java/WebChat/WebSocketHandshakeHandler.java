package WebChat;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;

public class WebSocketHandshakeHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

	private final String wsPath;
	private final ChannelHandler wsHandler;
	
	public WebSocketHandshakeHandler(String wsPath, ChannelHandler wsHandler) {
		super(false);
		this.wsHandler = wsHandler;
		this.wsPath = wsPath;
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest req) throws Exception {
		if(wsPath.equals(req.uri())) {
			handshareWebSocket(ctx, req);
		} else {
			ctx.fireChannelRead(req);
		}
	}

	private void handshareWebSocket(ChannelHandlerContext ctx, FullHttpRequest req) {
		try {
			WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory(wsPath, null, true);
			WebSocketServerHandshaker h = wsFactory.newHandshaker(req);
			if(h == null) {
				WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
			} else {
				h.handshake(ctx.channel(), req).addListener((ChannelFuture f)-> {
					ChannelPipeline p = f.channel().pipeline();
					p.replace(WebSocketHandshakeHandler.class, "wsHandler", wsHandler);
				});
			}
		} finally {
			req.release();
		}
	}
}
