package WebChat;

import TextBaseChat.ChatServerHandler;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PingWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;

public class WebChatHandler extends SimpleChannelInboundHandler<WebSocketFrame> {
	
	@Override
	public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
		ChannelPipeline p = ctx.pipeline();
		p.addLast(new WebSocketChatCodec());
		p.addLast(new ChatServerHandler());
		//p.addLast(handlerName(ctx), new ChatServerHandler());
	}
	
	private String handlerName(ChannelHandlerContext ctx) {
		final String[] result = new String[1];
		ctx.pipeline().toMap().forEach((name, handler)-> {
			if(handler.getClass().equals(this.getClass()))
				result[0] = name;
		});
		return result[0];
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame frame) throws Exception {
		if(frame instanceof CloseWebSocketFrame) {
			ctx.channel().writeAndFlush(frame.retain())
						 .addListener(ChannelFutureListener.CLOSE);
			return;
		}
		
		if( frame instanceof PingWebSocketFrame) {
			ctx.channel().writeAndFlush(new PongWebSocketFrame(frame.content().retain()));
			return;
		}
		
		if( !(frame instanceof TextWebSocketFrame) ) {
			throw new UnsupportedOperationException(String.format("%s frame types not supported", frame.getClass().getName()));
		}
		//WebChantHandler는 TextWebSocketFrame만 다음 핸들러에게 처리 위임
		//SimpleInboundChannelHandler<I>는 기본으로 release 처리가 들어있기 때문에 retain 호출이 필요
		ctx.fireChannelRead(frame.retain());
	}

}
