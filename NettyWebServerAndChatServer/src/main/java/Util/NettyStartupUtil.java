package Util;

import java.util.function.Consumer;

import TextBaseChat.ChatServerHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

public class NettyStartupUtil {
	public static void runServer(int port, ChannelHandler childHander, Consumer<ServerBootstrap> block) throws Exception {
		EventLoopGroup bossGroup = new NioEventLoopGroup(1);
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
			.channel(NioServerSocketChannel.class)
			.handler(new LoggingHandler(LogLevel.INFO))
			.childHandler(childHander);
			block.accept(b);
			Channel ch = b.bind(port).sync().channel();
			ch.closeFuture().sync();
		}finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}
	
	public static void runServer(int port, ChannelHandler childHandler) throws Exception {
		runServer(port, childHandler, b->{});
	}
	
	public static void runServer(int port, Consumer<ChannelPipeline> initializer) throws Exception {
		runServer(port, new ChannelInitializer<SocketChannel>() {
			@Override
			protected void initChannel(SocketChannel ch) throws Exception {
				initializer.accept(ch.pipeline());
			}
		});
	}
}
