package TextBaseChat;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.GlobalEventExecutor;

@Sharable
public class ChatServerHandler extends SimpleChannelInboundHandler<ChatMessage> {

	private static final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
	static final AttributeKey<String> nickAttr = AttributeKey.newInstance("nickname");
	private static final NicknameProvider nicknameProvider = new NicknameProvider();

	public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
		//이미 채널이 active인 상황에서 동적으로 이 핸들러가 등록되는 경우 channelActive는 호출되지 않음
		if(ctx.channel().isActive()) {
			helo(ctx.channel());
		}
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		helo(ctx.channel());
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		channels.remove(ctx.channel());
		channels.writeAndFlush(M("LEFT", nickname(ctx)));
		nicknameProvider.release(nickname(ctx));
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, ChatMessage msg) throws Exception {
		if("PING".equals(msg.command)) {
			//PING 명령어에 대한 응답
			ctx.write(msg);
		} else if("QUIT".equals(msg.command)) {
			//QUIT 명령어 처리하고 BYE 응답. 연결 종료
			ByteBufAllocator bufAlloc = ctx.alloc();
			ByteBuf returnBuf = bufAlloc.buffer();
			returnBuf.writeBytes("BYE".getBytes());
			ctx.writeAndFlush(returnBuf);
			ctx.close();
			//ctx.writeAndFlush(returnBuf).addListener(ChannelFutureListener.CLOSE);
			
			/**
			 * ctx.close()호출과 flush 후 addListener(ChannelFutureListener.CLOSE) 차이
    		    ChannelFutureListerner.CLOSE (해당 채널 작업 완료 시 close)
			       addListener로 등록된 채널이 작업 완료 이벤트 수신했을때 채널 종료. 내부적으로 future.channel().close() 호출
			    ctx.close() (ChannelPipeline에 등록된 채널들 close()
			       ChannelPipeline에 등록된 채널들을 종료. pipeline에 등록된 역순으로 종료
			 */
		} else if("SEND".equals(msg.command)) {
			//클라이언트로부터 대화 텍스트가 도착. 모든 채널에 FROM 메시지 방송
			channels.write(ctx.channel().attr(nickAttr).get()+" : " +msg.text);
		} else if("NICK".equals(msg.command)) {
			changeNickname(ctx, msg);
		} else {
			ctx.write(M("ERR", null, "unknown command->"+msg.command));
		}
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		ctx.flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		if(!ctx.channel().isActive()) {
			ctx.writeAndFlush(M("ERR", null, cause.getMessage())).addListener(ChannelFutureListener.CLOSE);
		}
	}

	private void helo(Channel ch) {
		if(nickname(ch) != null) return; //이미 지정된 경우
		String nick = nicknameProvider.reverse();
		if(nick == null) {
			ch.writeAndFlush(M("JOIN", nick)).addListener(ChannelFutureListener.CLOSE);
		} else {
			bindNickname(ch, nick);
			channels.forEach(c->ch.write(M("HAVE", nickname(c))));
			channels.writeAndFlush(M("JOIN", nick));
			channels.add(ch);
			ch.writeAndFlush(M("HELLO", nick));
		}
	}
	
	private void changeNickname(ChannelHandlerContext ctx, ChatMessage msg) {
		String newNick = msg.text.replace(" ", "_").replace(":", "-"); //.replace().replace() 성능?
		String prev = nickname(ctx);
		if(!newNick.equals(prev) && nicknameProvider.available(newNick)) {
			//nicknameProvider.release(prev).reverse(newNick);
			bindNickname(ctx.channel(), newNick);
			channels.writeAndFlush(M("NICK", prev, newNick));
		} else {
			ctx.write(M("ERR", null, "couldn't change"));
			
		}
	}
	
	private ChatMessage M(String... args) {
		switch(args.length) {
		case 1:
			return new ChatMessage(args[0]);
		case 2:
			return new ChatMessage(args[0], args[1]);
		case 3:
			ChatMessage m = new ChatMessage(args[0], args[1]);
			m.text = args[2];
			return m;
		default:
			throw new IllegalArgumentException();
		}
	}
	
	private void bindNickname(Channel c, String nickname) {
		c.attr(nickAttr).set(nickname);
	}
	
	private String nickname(ChannelHandlerContext ctx) {
		return nickname(ctx.channel());
	}
	
	private String nickname(Channel c) {
		return c.attr(nickAttr).get();
	}	
}
