package TextBaseChat.Promise;

import Util.NettyStartupUtil;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

public class PromiseServer {
	public static void main(String[] args) throws Exception {
		NettyStartupUtil.runServer(8031, (ChannelPipeline p)-> 
											p.addLast(new LineBasedFrameDecoder(1024),
													  new StringDecoder(),
													  new StringEncoder(),
													  new PromiseServerHandler()));
	}
}
