package TextBaseChat.Promise;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.concurrent.Promise;

public class PromiseServerHandler extends SimpleChannelInboundHandler<String> {

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String line) throws Exception {
		System.out.println(line);
		System.out.println("channelRead0:"+Thread.currentThread().getName());
		ctx.write("> " + line + "\n");
		Promise<String> p = ctx.executor().newPromise();
		new Thread(() -> {
			try {
				Thread.sleep(5000);
				System.err.println("new Thread:"+Thread.currentThread().getName());
				p.setSuccess("hello from " + Thread.currentThread().getName());
			} catch (InterruptedException e) { }
		}).start();
		
		p.addListener(f->{
			String s = "["+Thread.currentThread().getName()+"] listener got:"+f.get();
			System.out.println(s);
			ctx.writeAndFlush(">"+s+"\n");
		});
		
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ctx.flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
}
