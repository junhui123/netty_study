package Time;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelHandler.Sharable;

@Sharable
public class TimeServerHandler extends ChannelInboundHandlerAdapter {

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		ByteBuf time = ctx.alloc().buffer(4);
		time.writeInt((int) (System.currentTimeMillis() / 1000L + 2208988800L));
		
		ChannelFuture f=ctx.writeAndFlush(time);
		f.addListener(future-> {
			//참이 되는지 검증하는 부분을 확인하는 Boolean 표현식
			//컴파일 옵션(-ea)으로 assert를 사용 활성화 필요. 기본값은 검증 하지 않도록 되어 있음 
			assert f == future;
			ctx.close();
		});
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}

}
