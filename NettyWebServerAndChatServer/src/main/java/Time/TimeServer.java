package Time;

import Util.NettyStartupUtil;

public class TimeServer {
	public static void main(String[] args) throws Exception {
		NettyStartupUtil.runServer(8021, new TimeServerHandler());
	}
}
