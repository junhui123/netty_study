package WebChat;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.jupiter.api.Test;

import TextBaseChat.ChatServerHandler;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.embedded.EmbeddedChannel;

class WebChatHandlerTest {

	@Test
	void pipelineUpdate() throws Exception{
		EmbeddedChannel ch = new EmbeddedChannel(new WebChatHandler());
		ChannelPipeline p = ch.pipeline();
		assertThat("WebChatHandler는 유지돼야 합니다", p.first().getClass(), equalTo(WebChatHandler.class));
		assertThat("WebSocketChatCode이 추가돼야 해요", p.get(WebSocketChatCodec.class), notNullValue());
		assertThat("ChatServerHandler도 추가돼야 해요", p.get(ChatServerHandler.class), notNullValue());
	}
}
