package Http;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import io.netty.channel.FileRegion;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;

class httpStaticFileHandlerTest {

	@Test
	void indexAndNotFound() throws Exception {
		String index = HttpStaticServer.index;
		EmbeddedChannel ch = new EmbeddedChannel(
												 new HttpStaticFileHandler("/", index),
												 new HttpNotFoundHandler());
		ch.writeInbound(new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/"));
		
		HttpResponse res = ch.readOutbound();
		assertThat(res.status(), equalTo(HttpResponseStatus.OK));
		
		FileRegion content = ch.readOutbound();
		assertTrue(content.count() > 0 );
		assertThat(ch.readOutbound(), instanceOf(LastHttpContent.class));
		
		ch.writeInbound(new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/shuold-be-not-found"));
		HttpResponse res404 = ch.readOutbound();
		assertThat("404 응답 확인", res404, instanceOf(HttpResponse.class));
		assertThat("404 NOT FOUND 상태 코드 확인", res404.status(), equalTo(HttpResponseStatus.NOT_FOUND));
		
	}

}
