package main.com.nettyExample.ch4;

import java.nio.charset.Charset;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class EchoServerV1Handler extends ChannelInboundHandlerAdapter {

	@Override
	//입력된 메시지 그대로 반환
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		//네티 내부에서는 모든 데이터가 ByteBuf로 관리
		ByteBuf readMessage = (ByteBuf) msg;
		System.out.println("channelRead : "+readMessage.toString(Charset.defaultCharset()));
		ctx.writeAndFlush(msg);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
}
