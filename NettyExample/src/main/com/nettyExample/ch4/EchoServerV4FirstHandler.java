package main.com.nettyExample.ch4;

import java.nio.charset.Charset;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class EchoServerV4FirstHandler extends ChannelInboundHandlerAdapter {

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ByteBuf readMessage = (ByteBuf) msg;
		System.out.println("FirstHandler channelRead : " + readMessage.toString(Charset.defaultCharset()));
		ctx.write(msg);
		
		//이벤트를 수행하고나면 이벤트는 사라짐.
		//하나의 이벤트는 하나의 이벤트 메소드만 수행
		//다음 이벤트 핸들로러 이벤트를 전달하기 위해서 fire로 시작하는 fireXXXXX() 메소드 실행해서 이벤트를 전달
		//채널 파이프라인에서 이벤트 발생
		ctx.fireChannelRead(msg);
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		System.out.println("FirstHandler channelReadComplete");
		ctx.fireChannelReadComplete();
	}
}
