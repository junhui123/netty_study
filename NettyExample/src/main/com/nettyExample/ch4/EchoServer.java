package main.com.nettyExample.ch4;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import main.com.nettyExample.ch1.EchoServerHandler;

public class EchoServer {
	public static void main(String[] args) throws Exception {
		EventLoopGroup bossGroup = new NioEventLoopGroup(1);
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
			 .channel(NioServerSocketChannel.class)
 			 //childHandler를 통해 연결된 클라이언트 소켓 채널이 사용할 채널 파이프라인 설정 
			 .childHandler(new ChannelInitializer<SocketChannel>() { 
				@Override
				//클라이언트 소켓 채널이 생성될 때 자동으로 호출되어 채널 파이프라인 설정 수행
				protected void initChannel(SocketChannel ch) throws Exception {
					//네티 내부적으로 클라이언트 소켓 채널 생성 시 빈 채널 파이프라인 생성
					ChannelPipeline p = ch.pipeline();
					p.addLast(new EchoServerHandler());
				}
			});
			
			ChannelFuture f = b.bind(8888).sync();
			f.channel().closeFuture().sync();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}
}
