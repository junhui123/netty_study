package main.com.nettyExample.ch4;

import java.nio.charset.Charset;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class EchoServerV4SecondHandler extends ChannelInboundHandlerAdapter {

	//V4FirstHandler의 changeRead 이벤트만 수행
	//V4SecondHandler의 changeRead를 또한 수행하기 위해서는 V1에서 ctx.fireChangeRead(msg);
	//채널 파이프라인에서 changeRead 이벤트를 다시 발생 시켜야 한다.
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ByteBuf readMessage = (ByteBuf) msg;
		System.out.println("SecondHandler channelRead : " + readMessage.toString(Charset.defaultCharset()));
		//ctx.write(); 
	}
	
	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		System.out.println("SecondHandler channelReadComplete");
		ctx.flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
}
