package main.com.nettyExample.ch6;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

//자바는 기본적으로 unsigned 지원안함. unsigned 형식의 값은 원본 데이터 형식 보다 한 단계 더 큰 형식으로 변환
//더 큰 형식으로 변환으로 인해 생기는 메모리 낭비의 문제는 어쩔 수 없음.
class UnsignedByteBuffer {

	@Test
	public void unsignedBufferToJavaBuffer() {
		ByteBuf buf = Unpooled.buffer(11);
		buf.writeShort(-1);
		//unsigned byte는 한 단계 더 큰 short를 사용하여 변환
		assertEquals(65535, buf.getUnsignedShort(0));
	}
	
}
