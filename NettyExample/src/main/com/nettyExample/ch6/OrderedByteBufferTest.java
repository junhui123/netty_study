package main.com.nettyExample.ch6;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

import org.junit.jupiter.api.Test;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

class OrderedByteBufferTest {

	@Test
	public void pooledHeapBuffer() {
		ByteBuf buf = Unpooled.buffer();
		assertEquals(ByteOrder.BIG_ENDIAN, buf.order());
		
		buf.writeShort(1);

		//현재 버퍼의 읽기 인덱스 위치 표시
		buf.markReaderIndex();
		//저장된 데이터가 1인지 확인. 빅 엔디안으로 읽음
		assertEquals(1, buf.readShort());
		
		//markReadIndex()로 표시한 인덱스로 돌악가기
		buf.resetReaderIndex();
		
		//리틀 엔디안으로 변경 (네트워크 통신 규약 기본은 빅 엔디안. 특별한 상황에서만 리틀 엔디안 필요)
		ByteBuf littleEndianBuf = buf.order(ByteOrder.LITTLE_ENDIAN);
		//Ox0001 == 1 -> 0x0100 == 256
		assertEquals(256, littleEndianBuf.readShort());
	}
	
	final String source = "Hello World";
	@Test
	public void convertNettyBufferToJavaBuffer() {
		ByteBuf buf = Unpooled.buffer(11);
		buf.writeBytes(source.getBytes());
		assertEquals(source, buf.toString(Charset.defaultCharset()));
		
		//nioByteBuffer와 buf의 내부 배열은 공유
		ByteBuffer nioByteBuffer = buf.nioBuffer();
		assertNotNull(nioByteBuffer);
		
		String newStr = new String(nioByteBuffer.array(), nioByteBuffer.arrayOffset(), nioByteBuffer.remaining());
		assertEquals(source, newStr);
	}
	
	@Test
	public void convertJavaBufferToNettyBuffer() {
		ByteBuffer byteBuffer = ByteBuffer.wrap(source.getBytes());
		ByteBuf nettyBuffer = Unpooled.wrappedBuffer(byteBuffer);
		
		assertEquals(source, nettyBuffer.toString(Charset.defaultCharset()));
	}
	
	
}
