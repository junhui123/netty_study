package main.com.nettyExample.ch6;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.ByteBuffer;

import org.junit.jupiter.api.Test;

class JavaBufferTest {

	@Test
	void test() {
		ByteBuffer firstBuffer = ByteBuffer.allocateDirect(11);
		assertEquals(0,  firstBuffer.position());
		assertEquals(11,  firstBuffer.limit());
		
		firstBuffer.put((byte) 1);
		firstBuffer.put((byte) 2);
		firstBuffer.put((byte) 3);
		firstBuffer.put((byte) 4);
		
		assertEquals(4, firstBuffer.position());
		assertEquals(11, firstBuffer.limit());
		
		
		//flip() 호출 시  limit=position, poisition=0 으로 변경됨
		//새로운 값을 추가 또는 값 읽기의 경우 경우 0번째 인덱스부터 다시 시작함
		firstBuffer.flip();
		
		assertEquals(0, firstBuffer.position());
		assertEquals(4, firstBuffer.limit());
		for(int i=0; i<firstBuffer.limit(); i++) {
			System.out.println(firstBuffer.get(i));
		}
		
		firstBuffer.put((byte) 5);
		assertEquals(1, firstBuffer.position());
		assertEquals(4, firstBuffer.limit());
		
		firstBuffer.put((byte) 6);
		firstBuffer.put((byte) 7);
		firstBuffer.put((byte) 8);
		
		//limit 값을 변경하지 않으면 버퍼에 빈 공간이 있어도 추가 불가능
		firstBuffer.limit(11);
		firstBuffer.put((byte) 9);
		firstBuffer.put((byte) 10);
		System.out.println(firstBuffer.toString());
		
		for(int i=0; i<firstBuffer.limit(); i++) {
			System.out.println(firstBuffer.get(i));
		}
	}
}
