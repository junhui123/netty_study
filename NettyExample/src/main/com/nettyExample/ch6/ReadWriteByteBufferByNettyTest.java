package main.com.nettyExample.ch6;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

class ReadWriteByteBufferByNettyTest {

	@Test
	void testBuffer() {
		ByteBuf buf = PooledByteBufAllocator.DEFAULT.heapBuffer(11);
		
		assertEquals(11, buf.capacity());
		//assertEquals(true, buf.isDirect());
		assertEquals(false, buf.isDirect());
		
		buf.writeInt(65537);
		assertEquals(4, buf.readableBytes());
		assertEquals(7, buf.writableBytes());
		
		assertEquals(1, buf.readShort());
		assertEquals(2, buf.readableBytes());
		assertEquals(7, buf.writableBytes());
		
		assertEquals(true, buf.isReadable());
		
		printBuffer(buf);
		buf.clear(); //read, write 인덱스를 0으로 만듬
		assertEquals(0, buf.readableBytes());
		assertEquals(11, buf.writableBytes());
		printBuffer(buf, 1); //인덱스만 0이 될 뿐 값은 여전히 남아있음
		
		buf.writeInt(1234556); //0번째 write 인덱스부터 다시 덮어씀
		printBuffer(buf, buf.readableBytes());
	}
	
	private void printBuffer(ByteBuf buf, int count) {
		for (int i=0; i<count; i++) {
			System.out.print(buf.getInt(i)+" ");
		}
		System.out.println("");
	}
	
	private void printBuffer(ByteBuf buf) {
		for (int i=0; i<buf.readableBytes(); i++) {
			System.out.print(buf.getInt(i)+" ");
		}
		System.out.println("");
	}
}
