package main.com.nettyExample.ch6;

import java.nio.charset.Charset;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class EchoServerHandler extends ChannelInboundHandlerAdapter {

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ByteBuf readMessage = (ByteBuf) msg;
		System.out.println("channelRead :" + readMessage.toString(Charset.defaultCharset()));
		
		ByteBufAllocator byteBufAllocator = ctx.alloc();
		ByteBuf newBuffer = byteBufAllocator.buffer();
		
		//newBuffer 사용
		String source = "Hello World chapter6";
		newBuffer.writeBytes(source.getBytes());
		
		ctx.write(newBuffer);
	}
	
	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		ctx.flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
}
