package main.com.nettyExample.ch7;

import java.io.File;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;


public class httpSnoopServer {
	private static final int PORT = 8443;
	public static void main(String[] args) throws Exception {
		
		final String PATH = httpSnoopServer.class.getResource("").getPath();
		File certChainFile = new File(PATH+"\\netty.crt");
		File keyFile = new File(PATH+"\\privatekey.pem");
		
		//File certChainFile = new File("netty.crt");
		//File keyFile = new File("privatekey.pem");
		
		//newServerContext() is deprecated
		//SslContext sslCtx = SslContext.newServerContext(certChainFile, keyFile, "1234567890");
		SslContext sslCtx = SslContextBuilder.forServer(certChainFile, keyFile, "1234567890").build();

		EventLoopGroup bossGroup = new NioEventLoopGroup(1);
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
			.channel(NioServerSocketChannel.class)
			.handler(new LoggingHandler(LogLevel.INFO))
			.childHandler(new HttpSnoopServerInitializer(sslCtx));
				
			Channel ch = b.bind(PORT).sync().channel();
			ch.closeFuture().sync();
		} finally {
			
		}
	}
}
