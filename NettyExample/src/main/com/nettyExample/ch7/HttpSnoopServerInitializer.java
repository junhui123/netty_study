package main.com.nettyExample.ch7;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.ssl.SslContext;

public class HttpSnoopServerInitializer extends ChannelInitializer<SocketChannel> {

	private final SslContext sslCtx;
	
	public HttpSnoopServerInitializer(SslContext sslCtx) {
		this.sslCtx = sslCtx;
	}

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline p = ch.pipeline();
		
		if(sslCtx!=null) {
			p.addLast(sslCtx.newHandler(ch.alloc()));
		}
		p.addLast(new HttpRequestDecoder());
		//HTTP 요청/응답은 여러 파트로 구성될 수 있음.
		//이러한 파트를 연결해서 메시지로 구성을 해야 함.
		//메시지 파트를 FullHttpRequest와 FullHttpResponse로 병합하는 HttpObjectAggregator를 사용 하여 버퍼에 저장 후 
		//전달 가능해지면 전달
		p.addLast(new HttpObjectAggregator(1048576)); //최대 메시지 크기 지정 KB
		p.addLast(new HttpResponseEncoder());
		p.addLast(new HttpSnoopServerHandler());
	}

}
