package main.com.nettyExample.WebSocket;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;

public class TextWebSocketFrameHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

	private final ChannelGroup group;
	
	public TextWebSocketFrameHandler(ChannelGroup group) {
		this.group = group;
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) throws Exception {
		//메시지 참조 카운트 증가 시키고 ChannelGroup에 연결된 모든 클라이언트 write and flush
		//channelRead0()을 반환하는 순간 msg 참조 카운터가 감소 하므로 비동기식으로 나중에 완료될 수 있기 때문에 잘못된 참조에 대한 접근 방지
		group.writeAndFlush(msg.retain());  
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		//HTTP 연결이 업그레이드 되어 웹 소켓 연결되 되는 경우 HttpReqeustHandler를 제거
		if(evt == WebSocketServerProtocolHandler.ServerHandshakeStateEvent.HANDSHAKE_COMPLETE) {
			ctx.pipeline().remove(HttpRequestHandler.class);
			group.writeAndFlush(new TextWebSocketFrame("Client "+ctx.channel()+" joined"));
			group.add(ctx.channel()); //웹 소켓 채널을 ChannelGroup에 추가
		} else {
			super.userEventTriggered(ctx, evt);
		}
	}
}
