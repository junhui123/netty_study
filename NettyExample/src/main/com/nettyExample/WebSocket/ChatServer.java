package main.com.nettyExample.WebSocket;

import java.net.InetSocketAddress;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.concurrent.ImmediateEventExecutor;

public class ChatServer {
	private final ChannelGroup channelGroup = new DefaultChannelGroup(ImmediateEventExecutor.INSTANCE);	
	private final EventLoopGroup group = new NioEventLoopGroup();
	private Channel channel;
	
	public ChannelFuture start(InetSocketAddress address) {
		ServerBootstrap b = new ServerBootstrap();
		b.group(group)
		.channel(NioServerSocketChannel.class)
		.childHandler(createInitalizer(channelGroup));
		
		ChannelFuture future = b.bind(address);
		future.syncUninterruptibly(); //sync()와 차이?
		channel = future.channel();
		return future;
	}

	protected ChannelInitializer<Channel> createInitalizer(ChannelGroup group) {
		return new ChatServerInitializer(group);
	}
	
	public void destory() {
		if(channel != null) {
			channel.close();
		}
		channel.close();
		group.shutdownGracefully();
	}
	
	public static void main(String[] args) throws Exception {
		//if(args.length !=1 ) {
		//	System.err.println("Please give port as argument");
		//	System.exit(1);
		//}
		//int port = Integer.parseInt(args[0]);
		int port = 9999;
		final ChatServer endpoint = new ChatServer();
		ChannelFuture future = endpoint.start(new InetSocketAddress(port));
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				endpoint.destory();
			}
		});
		future.channel().closeFuture().syncUninterruptibly();
	}
}
