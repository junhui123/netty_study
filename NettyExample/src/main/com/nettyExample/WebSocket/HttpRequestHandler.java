package main.com.nettyExample.WebSocket;

import java.io.File;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.net.URL;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.DefaultFileRegion;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.stream.ChunkedNioFile;

/**
 *HTTP 요청과 응답을 관리
 */
public class HttpRequestHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

	private final String wsUri;
	private static File INDEX;
	
	static {
		URL location = HttpRequestHandler.class.getProtectionDomain().getCodeSource().getLocation();
		try {
			String path = location.toURI()+"index.html";
			path = !path.contains("file:") ? path : path.substring(5);
			INDEX = new File(path);
			if(!INDEX.exists()) {
				INDEX = new File("C:\\Users\\QWER\\eclipse-workspace\\NettyExample\\src\\main\\com\\nettyExample\\WebSocket\\index.html");
			}
		} catch(URISyntaxException e) {
			throw new IllegalStateException("Unable to locate index.html", e);
		}
	}
	
	public HttpRequestHandler(String usUri) {
		this.wsUri = usUri;
	}
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {
		if(wsUri.equalsIgnoreCase(request.getUri())) { //웹 소켓 업그레이드가 요청된 경우 참조 카운트 증가 시키고 다음 InboundHandler로 전달
			ctx.fireChannelRead(request.retain());
		} else {
			if(HttpHeaders.is100ContinueExpected(request)) { //Http1.1 기준으로  100 continue 요청 처리
				send100Continue(ctx);
			}
			RandomAccessFile file = new RandomAccessFile(INDEX, "r"); 
			HttpResponse response = new DefaultHttpResponse(request.getProtocolVersion(), HttpResponseStatus.OK);
			response.headers().set(HttpHeaders.Names.CONTENT_TYPE, "text/html; charset=UTF-8");
			boolean keepAlive = HttpHeaders.isKeepAlive(request);
			if(keepAlive) {
				response.headers().set(HttpHeaders.Names.CONTENT_LENGTH, file.length());
				response.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
			}
			ctx.write(response);
			
			//index.html을 클라이언트로 write.
			//암호화나 압축이 없는 경우 제로 카피를 통한 전송
			if(ctx.pipeline().get(SslHandler.class) == null) {
				//암호화 SSL/TLS가 없으므로 제로 카피 전송 
				ctx.write(new DefaultFileRegion(file.getChannel(), 0, file.length()));
			} else {
				ctx.write(new ChunkedNioFile(file.getChannel()));
			}
			
			ChannelFuture future = ctx.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);
			if(!keepAlive) { //Keep-Alive 요청이 없는 경우 마지막으로 버퍼에 쓰기 작업 시 Listener를  추가하고 연결 닫기 후 flush
				future.addListener(ChannelFutureListener.CLOSE);
			}
		}
	}
	
	//헤더 문제가 있다면 응답코드 100을 반환. (헤더가 Except: 100-continue를 전송 한 경우)
	private static void send100Continue(ChannelHandlerContext ctx) {
		FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.CONTINUE);
		ctx.writeAndFlush(response);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
}
