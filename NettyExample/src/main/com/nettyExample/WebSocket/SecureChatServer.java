package main.com.nettyExample.WebSocket;

import java.net.InetSocketAddress;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.group.ChannelGroup;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.SelfSignedCertificate;

/**
 * https://localhost:9999
 */
public class SecureChatServer extends ChatServer {
	private final SslContext context;

	public SecureChatServer(SslContext context) {
		this.context = context;
	}

	@Override
	protected ChannelInitializer<Channel> createInitalizer(ChannelGroup group) {
		
		ChannelOption.SO_BROADCAST;
		
		return new SecureChatServerInitializer(group, context);
	}
	
	public static void main(String[] args) throws Exception {
		//if(args.length !=1 ) {
		//	System.err.println("Please give port as argument");
		//	System.exit(1);
		//}
		//int port = Integer.parseInt(args[0]);
		int port = 9999;
		SelfSignedCertificate cert = new SelfSignedCertificate();
		SslContext context = SslContext.newServerContext(cert.certificate(), cert.privateKey());
		
		final SecureChatServer endpoint = new SecureChatServer(context);
		ChannelFuture future = endpoint.start(new InetSocketAddress(port));
		Runtime.getRuntime().addShutdownHook(new Thread() { //addShutdownHook() = 종료될때 이벤트를 가로채서 코드를 수행 후 셧다운 되도록 한다
			@Override
			public void run() {
				endpoint.destory();
			}
		});
		future.channel().closeFuture().syncUninterruptibly();
	}
}
