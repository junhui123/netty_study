package main.com.nettyExample.WebSocket;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.group.ChannelGroup;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

public class ChatServerInitializer extends ChannelInitializer<Channel>{

	private final ChannelGroup group;
	
	public ChatServerInitializer(ChannelGroup group) {
		this.group = group;
	}

	@Override
	protected void initChannel(Channel ch) throws Exception {
		ChannelPipeline p = ch.pipeline();
		p.addLast(new HttpServerCodec()); //Byte <--인/디코딩--> HttpRequest, HttpContent, LastHttpContent
		p.addLast(new ChunkedWriteHandler()); //파일의 내용 기록
		p.addLast(new HttpObjectAggregator(64*1024)); //분리된 Http 요청을 완전한 하나의 요청으로 만들어 다음 핸들러에 전달
		p.addLast(new HttpRequestHandler("/ws")); // /ws 요청이 아닌 경우 FullHttpReqeust 처리. /ws 인경우 웹 소켓 업그레이드 요청. Http 처리 핸들러 제거
		p.addLast(new WebSocketServerProtocolHandler("/ws")); //웹 소켓 업그레이드 요청  핸드세이크  처리. 파이프라인에 WebSocketFrameEncoder,Decoder 추가
		p.addLast(new TextWebSocketFrameHandler(group)); //TextWebSocketFrame 처리 및 핸드세이크 완료 이벤트 처리
	}
}
