package main.com.nettyExample.ch1;

import java.nio.Buffer;
import java.nio.charset.Charset;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class EchoServerHandler extends ChannelInboundHandlerAdapter {

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		String readMessage = ((ByteBuf) msg).toString(Charset.defaultCharset());
		System.out.println("수신한 문자열 [" + readMessage + "]");
		
		ByteBufAllocator byteBufAllocator = ctx.alloc();
		ByteBuf newBuffer = byteBufAllocator.buffer();
		
		//newBuffer 사용
		String returnMsg = "배고파";
		newBuffer.writeBytes(returnMsg.getBytes());
		
		msg = newBuffer;
		
		ctx.write(msg);
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		ctx.flush();
	}
	
}
