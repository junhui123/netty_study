package main.com.nettyExample.ch1;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class EchoClient {
	public static void main(String[] args) throws Exception {
		EventLoopGroup group = new NioEventLoopGroup();
		
		try {
			Bootstrap b  = new Bootstrap();	
			b.group(group)
			 .channel(NioSocketChannel.class)
			 .handler(new ChannelInitializer<SocketChannel>() {

				@Override
				protected void initChannel(SocketChannel ch) throws Exception {
					ChannelPipeline p = ch.pipeline();
					p.addLast(new EchoClientHandler());
				}
			});
			
			//비동기 입출력 connect 호출, 호출결과로 Future를 상속받은 ChannelFuture 반환
			//sync 메소드를 통해 완료 대기 혹은 예외 던짐. 완료를 받아야지 다음 라인 수행
			ChannelFuture f = b.connect("localhost", 8888).sync();
			
			f.channel().closeFuture().sync();
			
		} finally {
			group.shutdownGracefully();
		}
	}
}
