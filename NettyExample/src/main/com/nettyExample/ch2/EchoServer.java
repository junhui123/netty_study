package main.com.nettyExample.ch2;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import main.com.nettyExample.ch1.EchoServerHandler;

public class EchoServer {
	public static void main(String[] args) throws Exception {
		//클라이언트 연결 수락 하는 부모 스레드 그룹
		EventLoopGroup bossGroup = new NioEventLoopGroup(1); //생성자 숫자=생성할 스레드 갯수. 1=단일스레드
		//연결된 클라이언트 소켓으로 부터 데이터 입출력 및 이벤트 처리 담당 하는 자식 스레드
		EventLoopGroup workerGroup = new NioEventLoopGroup(); //서버 하드웨어 코어 기준 생성
		try {
			ServerBootstrap b = new ServerBootstrap();
			//서버 애플리케이션이 사용할 두 스레드 그룹 설정
			b.group(bossGroup, workerGroup)
			 .channel(NioServerSocketChannel.class) //서버 소켓이 사용할 네트워크 입출력 모드. NioServerSocketChannel 사용으로 NIO로 설정
			 .childHandler(new ChannelInitializer<SocketChannel>() { //자식 채널 초기화 
				@Override 
				//연결된 채컬이 초기화될때 기본 동작
				protected void initChannel(SocketChannel ch) throws Exception {
					ChannelPipeline p = ch.pipeline();
					//채널 파이프라인에 핸들러 클래스 등록. 클라이언트 연결이 생성되었을때 처리 담당
					p.addLast(new EchoServerHandler());
				}
			});

			ChannelFuture f = b.bind(8888).sync();
			
			f.channel().closeFuture().sync();
			
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}
}
