package main.com.nettyExample.ch2;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.socket.SocketChannel;
import main.com.nettyExample.ch1.EchoServerHandler;

//Epoll은 리눅스에서만 동작
public class EpollEchoServer {
	public static void main(String[] args) throws Exception {
		//epoll 입출력 모드를 위한 설정  스레드 그룹 설정
		EventLoopGroup bossGroup = new EpollEventLoopGroup(1);
		EventLoopGroup workerGroup = new EpollEventLoopGroup(); 
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
			//epoll 입출력 모드를 위한 설정  채널 설정
			 .channel(EpollServerSocketChannel.class)
			 .childHandler(new ChannelInitializer<SocketChannel>() {
				@Override 
				protected void initChannel(SocketChannel ch) throws Exception {
					ChannelPipeline p = ch.pipeline();
					p.addLast(new EchoServerHandler());
				}
			});

			ChannelFuture f = b.bind(8888).sync();
			
			f.channel().closeFuture().sync();
			
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}
}
