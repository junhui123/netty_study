package main.com.nettyExample.ch5;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.AttributeKey;

public class EchoServer {
	public static void main(String[] args) throws Exception {
		EventLoopGroup bossGroup = new NioEventLoopGroup(1);
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
			 .channel(NioServerSocketChannel.class)
			 .childHandler(new ChannelInitializer<SocketChannel>() { 
				@Override
				protected void initChannel(SocketChannel ch) throws Exception {
					ChannelPipeline p = ch.pipeline();
					p.addLast(new EchoServerHandler());
				}
			});
			
			//퓨처 패턴 구현체 ChannelFuture
			//ChannelFuture f = b.bind(8888).sync(); 
			//f.channel().closeFuture().sync();

			//포트 바인딩이 되기전 ChannelFuture 반환
			ChannelFuture bindFuture = b.bind(8888);
			//sync() = ChannelFuture 작업이 완료될때까지 블로킹. 작업이 완료되면 같이 완료
			bindFuture.sync(); //포트 바인딩이 완료될때까지 블로킹. 
			Channel serverChannel = bindFuture.channel(); //바인딩된 채널을 가져옴
			
			//채널이 생성될 때 CloseFuture 객체도 같이 생성. 연결 종료 이벤트를 받아 처리 
			//기본 생성된 CloseFuture 객체는 아무 동작도 설정되어 있지 않음
			ChannelFuture closeFuture = serverChannel.closeFuture(); 
			closeFuture.sync();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}
}
