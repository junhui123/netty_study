package main.com.nettyExample.ch5;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class EchoServerHandlerWithFuture extends ChannelInboundHandlerAdapter {

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ChannelFuture channelFuture = ctx.writeAndFlush(msg);
		final int writeMessageSize = ((ByteBuf) msg).readableBytes();
		
		//커스텀 채널 리스너 
		channelFuture.addListener(new ChannelFutureListener() {
			@Override
			//ChannelFuture 객체에서 발생하는 작업 완료 이벤트 메소드
			public void operationComplete(ChannelFuture future) throws Exception {
				System.out.println("전송한 Bytes : "+writeMessageSize);
				
				future.channel().closeFuture().addListener(new ChannelFutureListener() {
					@Override
					public void operationComplete(ChannelFuture future) throws Exception {
						System.out.println("CloseFuture 객체 연결 종료 이벤트 받아 처리 하는 부분 ");
					}
				});
				
				//ChannelFuture 객체에 포함된 Channel을 가져와 채널 닫기 이벤트 발생
				future.channel().close();
			}
		});
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
}
