package main.com.nettyExample.ch5;

import java.util.concurrent.Future;

/**
 * 퓨처 패턴
 * Future 객체  = 미래에 완료된 작업을 등록 처리 결과를 확인
 * 메서드를 호출 즉시 Future 객체를 반환
 * 작업이 완료되지 않았으면 다른일을 하고 완료여부를 지속적으로 확인 
 */
public class SpecialCake {
	public static void main(String[] args) {
		Bakery bakery = new Bakery();
		
		//케이크를 주문하고 주문서를 받는다. 
		Future future = bakery.orderCake();
		
		//다른일을 한다
		doSomething();
		
		//케이크 완성 확인
		if(future.isDone()) {
			Cake cake = ((BakeryFuture)future).getCake();
		} else {
			while(future.isDone() != true) {
				//다른 일을 한다.
				doSomething();
			}
			//while 루프를 빠져나왔으므로 완성된 케이크를 가져온다
			Cake cake = ((BakeryFuture)future).getCake();
		}
	}
	
	public static void doSomething() { }
}

class Bakery {
	public Future orderCake() {
		return null;
	}
}

class Cake {
	
}

interface BakeryFuture extends Future {
	public Cake getCake();
}
